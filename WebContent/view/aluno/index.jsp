<%@page import="entity.Nota"%>
<%@page import="entity.Matricula"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Aluno - Index</title>
	
	<script src="/SistemaDeNotas/scripts/jquery-3.4.1.js"></script>
	<script src="/SistemaDeNotas/scripts/view.js" type="text/javascript"></script>
	
	<link  href="/SistemaDeNotas/css/view.css" rel="stylesheet" type="text/css" media="all">
  	<link href="/SistemaDeNotas/css/main.css" rel="stylesheet">
</head>
<body>
	<img id="top" src="/SistemaDeNotas/images/top.png">
	<div id="form_container">
		<h1><a>Notas</a></h1>
		<div id="div-logo">
			<img id="img-logo" src="/SistemaDeNotas/images/logo_ifpb.png"/>
		</div>
		<br>
		<div id="disciplinas">
			<div class="form_description">
				<h2>Notas</h2>
			</div>
			<%
				Integer ano;
				Integer periodo;
				List<Matricula> matriculas = (List<Matricula>) request.getAttribute("matriculas");
				
				if(!matriculas.isEmpty()) {
					ano = matriculas.get(0).getDisciplina().getAno();
					periodo = matriculas.get(0).getDisciplina().getPeriodo();
					
					out.println("<h2>" + ano + "." + periodo + "</h2>");
					
					out.println("<table class=\"tg\">");
	
						out.println("<col width=\"425\">");
					  	out.println("<col width=\"50\">");
					  	out.println("<col width=\"50\">");
					  	out.println("<col width=\"50\">");
					  	out.println("<col width=\"50\">");
					  	out.println("<col width=\"90\">");
						out.println("<tr>");
							out.println("<th class=\"tg-0lax\">Nome</th>");
							out.println("<th class=\"tg-0lax\">N1</th>");
							out.println("<th class=\"tg-0lax\">N2</th>");
							out.println("<th class=\"tg-0lax\">N3</th>");
							out.println("<th class=\"tg-0lax\">M�dia</th>");
							out.println("<th class=\"tg-0lax\">Situa��o</th>");
						out.println("</tr>");
	
					for(Matricula matricula : matriculas) {
						if(ano.equals(matricula.getDisciplina().getAno()) && periodo.equals(matricula.getDisciplina().getPeriodo())) {
							
							out.println("<tr>");
								out.println("<td class=\"tg-0lax\">" + matricula.getDisciplina().getNome() + "</td>");
								for(Nota nota : matricula.getNotas()) {
									out.println("<td class=\"tg-0rax\">" + nota.getValor() + "</td>");
								}
								out.println("<td class=\"tg-0rax\">" + matricula.getMedia() + "</td>");
								out.println("<td class=\"tg-0lax\">" + matricula.getSituacao() + "</td>");
							out.println("</tr>");
			
						} else {
							out.println("</table>");
	
							ano = matricula.getDisciplina().getAno();
							periodo = matricula.getDisciplina().getPeriodo();
					
							out.println("<h2>" + ano + "." + periodo + "</h2>");
							
							out.println("<table class=\"tg\">");
	
								out.println("<col width=\"425\">");
							  	out.println("<col width=\"50\">");
							  	out.println("<col width=\"50\">");
							  	out.println("<col width=\"50\">");
							  	out.println("<col width=\"50\">");
							  	out.println("<col width=\"90\">");
								out.println("<tr>");
									out.println("<th class=\"tg-0lax\">Nome</th>");
									out.println("<th class=\"tg-0lax\">N1</th>");
									out.println("<th class=\"tg-0lax\">N2</th>");
									out.println("<th class=\"tg-0lax\">N3</th>");
									out.println("<th class=\"tg-0lax\">M�dia</th>");
									out.println("<th class=\"tg-0lax\">Situa��o</th>");
								out.println("</tr>");
								
								out.println("<tr>");
									out.println("<td class=\"tg-0lax\">" + matricula.getDisciplina().getNome() + "</td>");
									for(Nota nota : matricula.getNotas()) {
										out.println("<td class=\"tg-0rax\">" + nota.getValor() + "</td>");
									}
									out.println("<td class=\"tg-0rax\">" + matricula.getMedia() + "</td>");
									out.println("<td class=\"tg-0lax\">" + matricula.getSituacao() + "</td>");
								out.println("</tr>");
						}
					}
					out.println("</table>");
				}
			%>
		</div>
		<div id="footer">
			&copy; Jales Monteiro 2019
		</div>
	</div>	
	<img id="bottom" src="/SistemaDeNotas/images/bottom.png" alt="">
</body>
</html>