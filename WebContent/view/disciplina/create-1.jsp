<%@page import="entity.Disciplina"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Disciplina - Adicionar</title>
	
	<script src="/SistemaDeNotas/scripts/jquery-3.4.1.js"></script>
	<script src="/SistemaDeNotas/scripts/view.js" type="text/javascript"></script>
	
	<link  href="/SistemaDeNotas/css/view.css" rel="stylesheet" type="text/css" media="all">
  	<link href="/SistemaDeNotas/css/main.css" rel="stylesheet">
</head>
<body>
	<%
		Disciplina disciplina = (Disciplina) request.getAttribute("disciplina");
		String nome = disciplina.getNome() == null ? "" : disciplina.getNome();
		String ano = disciplina.getAno() == null ? "" : disciplina.getAno().toString();
		Integer periodo = disciplina.getPeriodo() == null ? 0 : disciplina.getPeriodo();
	%>
	
	<img id="top" src="/SistemaDeNotas/images/top.png">
	<div id="form_container">
		<h1><a>Adicionar Disciplina</a></h1>
		<div id="div-logo">
			<img id="img-logo" src="/SistemaDeNotas/images/logo_ifpb.png"/>
		</div>
		<br>
	
		<form method="post" action="/SistemaDeNotas/disciplina?acao=create&id=<%=request.getAttribute("id")%>&etapa=1" class="appnitro">
			<div class="form_description">
				<h2>Adicionar Disciplina</h2>
			</div>
			
			<div>
				<label for="nome" class="description">Nome</label>
				<input name="nome" value="<%=nome%>" type="text" maxlength="255" class="element text medium"/>
			</div>
	
			<div>
				<label for="ano" class="description">Ano</label>
				<input name="ano" value="<%=ano%>" class="element text medium"/>
			</div>
			
			<div>
				<label for="periodo" class="description">Per�odo</label>
				<select name="periodo" class="element text medium">
					<option value="" <%= periodo == 0 ? "selected=\"selected\"" : "" %>></option>
					<option value="1" <%= periodo == 1 ? "selected=\"selected\"" : "" %>>1</option>
					<option value="2" <%= periodo == 2 ? "selected=\"selected\"" : "" %>>2</option>
					<option value="3" <%= periodo == 3 ? "selected=\"selected\"" : "" %>>3</option>
					<option value="4" <%= periodo == 4 ? "selected=\"selected\"" : "" %>>4</option>
				</select>
			</div>
			
			 <input type="hidden" name="form" value="1">
	
			<div class="buttons">
				<button type="submit" class="button_text">Avan�ar</button>
				<button type="button" onclick="location.href='/SistemaDeNotas/professor?acao=index&id=<%=request.getAttribute("id")%>';" class="button_text">Voltar</button>
			</div>
		</form>
		<div id="footer">
			&copy; Jales Monteiro 2019
		</div>
	</div>
	<img id="bottom" src="/SistemaDeNotas/images/bottom.png" alt="">
</body>
</html>