<%@page import="entity.Aluno"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Disciplina - Adicionar</title>
	<script src="/SistemaDeNotas/scripts/view.js" type="text/javascript"></script>
	
	<link  href="/SistemaDeNotas/css/view.css" rel="stylesheet" type="text/css" media="all">
	<link href="/SistemaDeNotas/css/fontawesome.css" rel="stylesheet">
	<link href="/SistemaDeNotas/css/brands.css" rel="stylesheet">
  	<link href="/SistemaDeNotas/css/solid.css" rel="stylesheet">
  	<link href="/SistemaDeNotas/css/main.css" rel="stylesheet">
</head>
<body>
	<%
		List<Aluno> alunos = (List<Aluno>) request.getAttribute("alunos");
		Boolean erro = (Boolean) request.getAttribute("erro");
		Boolean sucesso = (Boolean) request.getAttribute("sucesso");
	%>
	<img id="top" src="/SistemaDeNotas/images/top.png">
	<div id="form_container">
		<h1><a>Adicionar Disciplina</a></h1>
		<div id="div-logo">
			<img id="img-logo" src="/SistemaDeNotas/images/logo_ifpb.png"/>
		</div>
		
		<% if(erro != null && erro) {%>
			<div class="alert mensagem-erro">
				<i class="fas fa-exclamation"></i> Aluno n�o encontrado.
			</div>
		<% } else if(sucesso != null && sucesso) {%>
			<div class="alert mensagem-sucesso">
				<i class="fas fa-check"></i> Aluno adicionado.
			</div>
		<% } %>
		
		<div id="disciplinas">
			<div class="form_description">
				<h2>Alunos Cadastrados</h2>
			</div>
			
			<% if(alunos.isEmpty()) { %>
				<span>Sem alunos.</span>
			<% } else { %>
				<table class="tg">
					<tr>
						<th>Nome</th>
						<th>Email</th>
						<th>A��es</th>
					</tr>
					<c:forEach items="${alunos}" var="aluno">
						<tr>
							<td class="tg-0lax">${aluno.nome}</td>
							<td class="tg-0lax">${aluno.email}</td>
							<td class="tg-0lax"><a href="#"><i class="fas fa-minus"></i></a></td>
						</tr>
					</c:forEach>
				</table>
			<% } %>
		</div>
		
		<form method="post" action="/SistemaDeNotas/disciplina?acao=create&id=<%=request.getAttribute("id")%>&etapa=2" class="appnitro">
			<div class="form_description">
				<h2>Adicionar Disciplina</h2>
			</div>
			
			<div>
				<label for="email" class="description">Email</label>
				<input name="email" type="text" maxlength="255" class="element text medium"/>
			</div>
		
		 	<input type="hidden" name="form" value="1">
			
			<div class="buttons">
				<button type="submit" class="button_text">Adicionar aluno</button>
				<button type="button" onclick="location.href='/SistemaDeNotas/disciplina?acao=create&id=<%=request.getAttribute("id")%>&etapa=3';" class="button_text">Adicionar disciplina</button>
				<button type="button" onclick="location.href='/SistemaDeNotas/disciplina?acao=create&id=<%=request.getAttribute("id")%>&etapa=1';" class="button_text">Voltar</button>
			</div>	
		</form>
		
		<div id="footer">
			&copy; Jales Monteiro 2019
		</div>
	</div>
	<img id="bottom" src="/SistemaDeNotas/images/bottom.png" alt="">
</body>
</html>