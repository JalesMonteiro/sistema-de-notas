<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Disciplina - View</title>
  	<link href="/SistemaDeNotas/css/main.css" rel="stylesheet">
</head>
<body>Disciplina Controller -> View JSP
	
	<h1>${disciplina.ano}.${disciplina.periodo} - ${disciplina.nome}</h1>
	
	<table class="tg">
		<tr>
	    	<th class="tg-1lax title" colspan="3">Alunos</td>
		</tr>
		<c:forEach items="${disciplina.alunos}" var="aluno">
			<tr>
				<td class="tg-0lax" colspan="3">${aluno.nome}</td>
			</tr>
		</c:forEach>
	</table>
	
	<br>
	
	<div class="buttons">
		<button type="button" onclick="location.href='/SistemaDeNotas/professor?acao=index&id=<%=request.getAttribute("id")%>';">Voltar</button>
	</div>

</body>
</html>