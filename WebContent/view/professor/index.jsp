<%@page import="entity.Disciplina"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Professor - Index</title>
	
	<script src="/SistemaDeNotas/scripts/jquery-3.4.1.js"></script>
	<script src="/SistemaDeNotas/scripts/view.js" type="text/javascript"></script>
	
	<link  href="/SistemaDeNotas/css/view.css" rel="stylesheet" type="text/css" media="all">
	<link href="/SistemaDeNotas/css/fontawesome.css" rel="stylesheet">
	<link href="/SistemaDeNotas/css/brands.css" rel="stylesheet">
  	<link href="/SistemaDeNotas/css/solid.css" rel="stylesheet">
  	<link href="/SistemaDeNotas/css/main.css" rel="stylesheet">
</head>
<body>
	<img id="top" src="/SistemaDeNotas/images/top.png">
	<div id="form_container">
		<h1><a>Disciplinas</a></h1>
		<div id="div-logo">
			<img id="img-logo" src="/SistemaDeNotas/images/logo_ifpb.png"/>
		</div>
		<br>
		<div id="disciplinas">
			<div class="form_description">
				<h2>Disciplinas</h2>
			</div>
		
			<%
				Integer ano;
				Integer periodo;
				List<Disciplina> disciplinas = (List<Disciplina>) request.getAttribute("disciplinas");
				if(!disciplinas.isEmpty()) {
					ano = disciplinas.get(0).getAno();
					periodo = disciplinas.get(0).getPeriodo();
					
					out.println("<h2>" + ano + "." + periodo + "</h2>");
					
					out.println("<table class=\"tg\">");
	
						out.println("<col width=\"425\">");
					  	out.println("<col width=\"100\">");
					  	out.println("<col width=\"80\">");
						out.println("<tr>");
							out.println("<th class=\"tg-0lax\">Nome</th>");
							out.println("<th class=\"tg-0lax\">N� de Alunos</th>");
							out.println("<th class=\"tg-0lax\">A��es</th>");
						out.println("</tr>");
					
					for(Disciplina disciplina : disciplinas) {
						if(ano.equals(disciplina.getAno()) && periodo.equals(disciplina.getPeriodo())) {
							
							out.println("<tr id=\"disciplina-" + disciplina.getId() + "\">");
								out.println("<td class=\"tg-0lax\">" + disciplina.getNome() + "</td>");
								out.println("<td class=\"tg-0lax\">" + disciplina.getAlunosQtd() + "</td>");
								out.println("<td class=\"tg-0lax\">");
									out.println("<a href=\"/SistemaDeNotas/disciplina?acao=view&id=" + disciplina.getId() + "\"><i class=\"fas fa-eye\"></i></a>");
									out.println("<a href=\"/SistemaDeNotas/disciplina?acao=update&id=" + disciplina.getId() + "\"><i class=\"fa fa-pen\"></i></a>");
									out.println("<a href=\"/SistemaDeNotas/disciplina?acao=delete&id=" + disciplina.getId() + "\" class=\"delete-link\"><i class=\"fas fa-trash\"></i></a>");
									out.println("<a href=\"/SistemaDeNotas/disciplina?acao=notas&id=" + disciplina.getId() + "\"><i class=\"fas fa-check\"></i></a>");
								out.println("</td>");
							out.println("</tr>");
							
						} else {
							out.println("</table>");
							
							ano = disciplina.getAno();
							periodo = disciplina.getPeriodo();
					
							out.println("<h2>" + ano + "." + periodo + "</h2>");
							
							out.println("<table class=\"tg\">");
	
								out.println("<col width=\"425\">");
							  	out.println("<col width=\"100\">");
							  	out.println("<col width=\"80\">");
							  	
								out.println("<tr>");
									out.println("<th class=\"tg-0lax\">Nome</th>");
									out.println("<th class=\"tg-0lax\">N� de Alunos</th>");
									out.println("<th class=\"tg-0lax\">A��es</th>");
								out.println("</tr>");
								
								out.println("<tr id=\"disciplina-" + disciplina.getId() + "\">");
									out.println("<td class=\"tg-0lax\">" + disciplina.getNome() + "</td>");
									out.println("<td class=\"tg-0lax\">" + disciplina.getAlunosQtd() + "</td>");
									out.println("<td class=\"tg-0lax\">");
										out.println("<a href=\"/SistemaDeNotas/disciplina?acao=view&id=" + disciplina.getId() + "\"><i class=\"fas fa-eye\"></i></a>");
										out.println("<a href=\"/SistemaDeNotas/disciplina?acao=update&id=" + disciplina.getId() + "\"><i class=\"fa fa-pen\"></i></a>");
										out.println("<a href=\"/SistemaDeNotas/disciplina?acao=delete&id=" + disciplina.getId() + "\" class=\"delete-link\"><i class=\"fas fa-trash\"></i></a>");
										out.println("<a href=\"/SistemaDeNotas/disciplina?acao=notas&id=" + disciplina.getId() + "\"><i class=\"fas fa-check\"></i></a>");
									out.println("</td>");
								out.println("</tr>");
						}
					}
					out.println("</table>");
				}
			%>
			<br>
			<div class="buttons">
				<input class="button_text" type="button" value="Adicionar disciplina" onclick="javascript:window.location.href='/SistemaDeNotas/disciplina?acao=create&id=<%=request.getAttribute("id")%>&etapa=1'"/>
			</div>
		</div>
		<div id="footer">
			&copy; Jales Monteiro 2019
		</div>
		<script type="text/javascript">
			$("a.delete-link").click(function(event){
				event.preventDefault();
				var element = event.target;
	
				$.post(this.href,
					function(data, status) {
						if(status == "success") {
							$(element).closest('tr').remove();
						}
					}
				);
		
			    return false;
			});
		</script>
	</div>
	<img id="bottom" src="/SistemaDeNotas/images/bottom.png" alt="">
</body>
</html>