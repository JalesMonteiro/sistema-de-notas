package bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import entity.Aluno;

@Stateless
public class AlunoBean {
	@EJB
	DBHelper dbhelper;

    /**
     * Default constructor. 
     */
    public AlunoBean() {
        // TODO Auto-generated constructor stub
    }
    
    public Aluno find(int id) {
    	EntityManager em = dbhelper.getEntityManager();
    	
		return em.find(Aluno.class, id);
    }

}
