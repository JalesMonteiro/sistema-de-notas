package bean;

import javax.ejb.Singleton;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Session Bean implementation class DBHelper
 */
@Singleton
public class DBHelper {
	
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("aulaEJB");
	
    public DBHelper() {}
    
    @Produces
    public EntityManager getEntityManager() {
    	return emf.createEntityManager();
    }
    
    public void close(@Disposes EntityManager em) {
    	if(em.isOpen()) {
    		em.close();
    	}
    }
}
