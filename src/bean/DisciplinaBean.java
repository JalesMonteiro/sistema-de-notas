package bean;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Disciplina;

/**
 * Session Bean implementation class AdicionarDisciplinaBean
 */
@Stateless
public class DisciplinaBean {
	@EJB
	DBHelper dbhelper;

    /**
     * Default constructor. 
     */
    public DisciplinaBean() {
    }
    
    public List<Disciplina> findAllByProfessorId(int id) {
    	EntityManager em = dbhelper.getEntityManager();
    	TypedQuery<Disciplina> query = em.createNamedQuery("Disciplina.findAllByProfessor", Disciplina.class);
		query.setParameter("professor_id", id);
		List<Disciplina> disciplinas = query.getResultList();

		for(Disciplina model : disciplinas) {
			TypedQuery<Long> query2 = em.createQuery("SELECT COUNT(m) FROM Matricula m WHERE disciplina_id = :disciplina_id", Long.class);
			long qtd = query2.setParameter("disciplina_id", model.getId()).getSingleResult();
			model.setAlunosQtd(qtd);
        }
		
		return disciplinas;
    }

	public Disciplina find(Integer id) {
		EntityManager em = dbhelper.getEntityManager();
		return em.find(Disciplina.class, id);
	}

}
