package bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import entity.Aluno;
import entity.Disciplina;

/**
 * Session Bean implementation class AdicionarDisciplinaBean
 */
@Stateful
public class DisciplinaCreateBean {
	@EJB
	DBHelper dbhelper;
	@EJB
	ProfessorBean professorBean;

	Disciplina disciplina;
	List<Aluno> alunos;
    /**
     * Default constructor. 
     */
    public DisciplinaCreateBean() {
        disciplina = new Disciplina();
        alunos = new ArrayList<Aluno>();
    }
    
    public void setAno(Integer ano) {
    	disciplina.setAno(ano);
    }
    
    public void setPeriodo(Integer periodo) {
    	disciplina.setPeriodo(periodo);
    }
    
    public void setNome(String nome) {
    	disciplina.setNome(nome);
    }
    
    public void setProfessor(Integer professor_id) {
    	disciplina.setProfessor(professorBean.find(professor_id));
    }
    
    public Boolean adicionarAluno(String email) {
    	EntityManager em = dbhelper.getEntityManager();
    	
    	TypedQuery<Aluno> query = em.createNamedQuery("Aluno.findByEmail", Aluno.class);
    	
    	try {
    		Aluno aluno = query.setParameter("email", email).getSingleResult();;
    		alunos.add(aluno);
    		return true;
    	} catch (NoResultException e) {
			return false;
		}
    }
    
    public void removerAluno(String email) {
    	Iterator<Aluno> itr = alunos.iterator();
    	
    	while(itr.hasNext()) {
    		Aluno aluno = (Aluno) itr.next();
    		
    		if( aluno.getEmail().equals(email) ) {
    			itr.remove();
    		}
    	}
    }
    
    public Disciplina getDisciplina() {
    	return disciplina;
    }
    
    public List<Aluno> getAlunos() {
    	return alunos;
    }
    
    public void salvar() {
    	EntityManager em = dbhelper.getEntityManager();
    	
    	disciplina.setAlunos(alunos);
    	em.persist(disciplina);
    }

}
