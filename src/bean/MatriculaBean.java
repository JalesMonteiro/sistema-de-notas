package bean;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Matricula;

@Stateless
public class MatriculaBean {
	@EJB
	DBHelper dbhelper;

    /**
     * Default constructor. 
     */
    public MatriculaBean() {
        // TODO Auto-generated constructor stub
    }
    
    public List<Matricula> findAllByAlunoId(int id) {
    	EntityManager em = dbhelper.getEntityManager();
    	
    	TypedQuery<Matricula> query = em.createQuery("SELECT m FROM Matricula m LEFT JOIN Disciplina d ON disciplina_id = id WHERE aluno_id = :aluno_id ORDER BY ano DESC, periodo DESC, nome ASC", Matricula.class);
		query.setParameter("aluno_id", id);
		List<Matricula> matriculas = query.getResultList();
		return matriculas;
    }

}
