package bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

import entity.Professor;

@Stateless
public class ProfessorBean {
	@EJB
	DBHelper dbhelper;

    /**
     * Default constructor. 
     */
    public ProfessorBean() {
        // TODO Auto-generated constructor stub
    }
    
    public Professor find(int id) {
    	EntityManager em = dbhelper.getEntityManager();
    	
    	Professor professor = em.find(Professor.class, id);
		return professor;
    }

}
