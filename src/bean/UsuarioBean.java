package bean;

import java.util.concurrent.TimeUnit;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import entity.Usuario;

//definir o tempo da sessao
@Stateful
@StatefulTimeout(value = 30, unit = TimeUnit.MINUTES)
public class UsuarioBean {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("aulaEJB");
	private EntityManager em = emf.createEntityManager();

	Usuario usuario = null;
    /**
     * Default constructor. 
     */
    public UsuarioBean() {
        // TODO Auto-generated constructor stub
    }
    
    public Boolean login(String email, String senha) {
    	if(validate(email, senha)) {
    		usuario = getUsuario(email);
    		return true;
    	}
    	
    	return false;
    }
    
    private Usuario getUsuario(String email) {
    	TypedQuery<Usuario> query = em.createNamedQuery("Usuario.findByEmail", Usuario.class);
		query.setParameter("email", email);
		Usuario usuario;
		
		try {
			usuario = query.getSingleResult();
		} catch (NoResultException e) {
			usuario = null;
		}
    	
		return usuario;
    }
    
    public Boolean validate(String email, String senha) {
    	Usuario usuario = getUsuario(email);
    	
    	if(senha.equals(usuario.getSenha())) {
    		return true;
    	}
    	
    	return false;
    }
    
    public Boolean isLoged() {
    	return usuario != null;
    }
    
    public Usuario getUsuario() {
    	return usuario;
    }
    
    public Integer getId() {
    	return usuario.getId();
    }
    
    @Remove
    public void logout() {
    	
    }
}
