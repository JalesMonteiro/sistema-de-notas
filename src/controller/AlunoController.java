package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.AlunoBean;
import bean.MatriculaBean;
import bean.UsuarioBean;



/**
 * Servlet implementation class AlunoController
 */
@WebServlet("/aluno")
public class AlunoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@EJB
	MatriculaBean matriculaBean;
	@EJB
	UsuarioBean usuarioBean;
	@EJB
	AlunoBean alunoBean;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlunoController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		resolverRequisicao(request, response);
	}

	private void resolverRequisicao(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String acao = request.getParameter("acao");
		Boolean acaoValida = false;
		RequestDispatcher rd = null;
		
		switch (acao) {
			case "index":
				rd = index(request, response);
				acaoValida = true;
				break;
		}
		
		if(acaoValida) {
			rd.forward(request, response);
		} else {
			PrintWriter out = response.getWriter();
			out.println("<html><body>P�gina inv�lida.</body></html>");
		}
	}

	private RequestDispatcher index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.valueOf(request.getParameter("id"));
		if(id >= 1 && id <= 3) {
			request.setAttribute("matriculas", matriculaBean.findAllByAlunoId(id));
		}
		
		return request.getRequestDispatcher("/view/aluno/index.jsp");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
