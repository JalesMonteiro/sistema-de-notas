package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.DisciplinaBean;
import bean.DisciplinaCreateBean;

/**
 * Servlet implementation class ProfessorController
 */
@WebServlet("/disciplina")
public class DisciplinaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	DisciplinaBean disciplinaBean;
	@EJB
	DisciplinaCreateBean disciplinaCreateBean;
	
	private Integer id;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisciplinaController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		resolverRequisicao(request, response);
	}

	private void resolverRequisicao(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		id = Integer.valueOf(request.getParameter("id"));
		String acao = request.getParameter("acao");
		Boolean acaoValida = false;
		RequestDispatcher rd = null;
		
		request.setAttribute("id", id);
		
		switch (acao) {
			case "create":
				rd = create(request, response);
				acaoValida = true;
				break;
			case "update":
				rd = update(request, response);
				acaoValida = true;
				break;
			case "view":
				rd = view(request, response);
				acaoValida = true;
				break;
		}
		
		if(acaoValida && rd != null) {
			rd.forward(request, response);
		} else {
			PrintWriter out = response.getWriter();
			out.println("<html><body>P�gina inv�lida.</body></html>");
		}
	}

	private RequestDispatcher create(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String etapa_p = request.getParameter("etapa");
		
		if(etapa_p != null) {
			Integer etapa = Integer.valueOf(etapa_p);
		
			switch (etapa) {
				case 1:
					return create1(request, response);
				case 2:
					return create2(request, response);
			}
		}
		
		response.sendRedirect(request.getContextPath() + "/disciplina?acao=create&id="+id+"&etapa=1");
		return null;
	}

	private RequestDispatcher create1(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String form = request.getParameter("form");
		
		if(form != null && form.equals("1")) {
			disciplinaCreateBean.setNome(request.getParameter("nome"));
			
			if(!request.getParameter("ano").isEmpty()) {
				disciplinaCreateBean.setAno(Integer.valueOf(request.getParameter("ano")));
			}
			
			if(!request.getParameter("periodo").isEmpty()) {
				disciplinaCreateBean.setPeriodo(Integer.valueOf(request.getParameter("periodo")));
			}

			response.sendRedirect(request.getContextPath() + "/disciplina?acao=create&id="+id+"&etapa=2");
			return null;
		} else {
			request.setAttribute("disciplina", disciplinaCreateBean.getDisciplina());
			return request.getRequestDispatcher("/view/disciplina/create-1.jsp");
		}
	}

	private RequestDispatcher create2(HttpServletRequest request, HttpServletResponse response) {
		String form = request.getParameter("form");
		
		if(form != null && form.equals("1")) {
			if(disciplinaCreateBean.adicionarAluno(request.getParameter("email")) ) {
				request.setAttribute("sucesso", true);
			} else {
				request.setAttribute("erro", true);
			}
		}
		
		request.setAttribute("alunos", disciplinaCreateBean.getAlunos());
		return request.getRequestDispatcher("/view/disciplina/create-2.jsp");
	}

	private RequestDispatcher update(HttpServletRequest request, HttpServletResponse response) {
		return request.getRequestDispatcher("/view/disciplina/update.jsp");
	}

	private RequestDispatcher view(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("disciplina", disciplinaBean.find(Integer.valueOf(request.getParameter("id"))));
		
		return request.getRequestDispatcher("/view/disciplina/view.jsp");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
