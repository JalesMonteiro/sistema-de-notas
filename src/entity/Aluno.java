package entity;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the aluno database table.
 * 
 */
@Entity
@PrimaryKeyJoinColumn(name = "usuario_id")
@NamedQuery(name="Aluno.findByEmail", query="SELECT a FROM Aluno a WHERE email = :email")
public class Aluno extends Usuario {
	private static final long serialVersionUID = 1L;

	//bi-directional many-to-many association to Disciplina
	@ManyToMany(mappedBy="alunos")
	private List<Disciplina> disciplinas;

	public Aluno() {
	}

	public List<Disciplina> getDisciplinas() {
		return this.disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

}