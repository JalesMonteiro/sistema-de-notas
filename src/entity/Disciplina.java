package entity;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the disciplina database table.
 * 
 */
@Entity
@NamedQuery(name="Disciplina.findAll", query="SELECT d FROM Disciplina d")
@NamedQuery(name="Disciplina.findAllByProfessor", query="SELECT d FROM Disciplina d WHERE professor_id = :professor_id ORDER BY ano DESC, periodo DESC")
public class Disciplina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private Integer ano;

	private Integer periodo;

	private String nome;

	//bi-directional many-to-one association to Professor
	@ManyToOne
	@JoinColumn(name="professor_id")
	private Professor professor;

	//bi-directional many-to-many association to Aluno
	@ManyToMany
	@JoinTable(
		name="matricula"
		, joinColumns={
			@JoinColumn(name="disciplina_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="aluno_id")
			}
		)
	private List<Aluno> alunos;
	
	@Transient
	private long alunosQtd = 0;

	public Disciplina() {}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getAno() {
		return this.ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public Professor getProfessor() {
		return this.professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public List<Aluno> getAlunos() {
		return this.alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public long getAlunosQtd() {
		return alunosQtd;
	}

	public void setAlunosQtd(long alunosQtd) {
		this.alunosQtd = alunosQtd;
	}

}