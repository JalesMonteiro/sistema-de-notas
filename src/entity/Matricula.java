package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the matricula database table.
 * 
 */
@Entity
@NamedQuery(name="Matricula.findAll", query="SELECT m FROM Matricula m")
@NamedQuery(name="Matricula.findAllByAluno", query="SELECT m FROM Matricula m WHERE aluno_id = :aluno_id")
public class Matricula implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MatriculaPK id;

	//bi-directional one-to-one association to Usuario
	@OneToOne
	@JoinColumn(name = "disciplina_id", referencedColumnName = "id", insertable = false, updatable = false, nullable=false)
	private Disciplina disciplina;
	
	//bi-directional many-to-one association to Nota
	@OneToMany(mappedBy="matricula")
	private List<Nota> notas;

	public Matricula() {
	}

	public MatriculaPK getId() {
		return this.id;
	}

	public void setId(MatriculaPK id) {
		this.id = id;
	}

	public Disciplina getDisciplina() {
		return this.disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public List<Nota> getNotas() {
		return this.notas;
	}

	public void setNotas(List<Nota> notas) {
		this.notas = notas;
	}

	public Nota addNota(Nota nota) {
		getNotas().add(nota);
		nota.setMatricula(this);

		return nota;
	}

	public Nota removeNota(Nota nota) {
		getNotas().remove(nota);
		nota.setMatricula(null);

		return nota;
	}

	public String getNotasString() {
		String notasStr = "";
		
		if(!notas.isEmpty()) {
			for (Nota nota : notas) {				// gera uma string de notas: 10,0 | 8,0 | 7,5
				notasStr += nota.getValor()+" | ";
			}
			notasStr = notasStr.substring(0, notasStr.length() - 3);
		}
		
		return notasStr;
	}

	public BigDecimal getMedia() {
		BigDecimal total = BigDecimal.ZERO;
		
		if(!notas.isEmpty()) {
			for (Nota nota : notas) {
				total = total.add(nota.getValor());
			}
		}
		
		return total.divide(BigDecimal.valueOf(3), RoundingMode.HALF_EVEN);
	}

	public String getSituacao() {
		BigDecimal media = getMedia();
		
		if(media.compareTo(BigDecimal.valueOf(70)) > -1) {
			return "Aprovado ";
		} else if(media.compareTo(BigDecimal.valueOf(40)) < 0) {
			return "Reprovado ";
		} else {
			return "Prova Final ";
		}
	}

}