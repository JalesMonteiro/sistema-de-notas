package entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the matricula database table.
 * 
 */
@Embeddable
public class MatriculaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="disciplina_id", insertable=false, updatable=false)
	private int disciplinaId;

	@Column(name="aluno_id", insertable=false, updatable=false)
	private int alunoId;

	public MatriculaPK() {
	}
	public int getDisciplinaId() {
		return this.disciplinaId;
	}
	public void setDisciplinaId(int disciplinaId) {
		this.disciplinaId = disciplinaId;
	}
	public int getAlunoId() {
		return this.alunoId;
	}
	public void setAlunoId(int alunoId) {
		this.alunoId = alunoId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MatriculaPK)) {
			return false;
		}
		MatriculaPK castOther = (MatriculaPK)other;
		return 
			(this.disciplinaId == castOther.disciplinaId)
			&& (this.alunoId == castOther.alunoId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.disciplinaId;
		hash = hash * prime + this.alunoId;
		
		return hash;
	}
}