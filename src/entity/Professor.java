package entity;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the professor database table.
 * 
 */
@Entity
@PrimaryKeyJoinColumn(name = "usuario_id")
@NamedQuery(name="Professor.findAll", query="SELECT p FROM Professor p")
public class Professor extends Usuario {
	private static final long serialVersionUID = 1L;

	//bi-directional many-to-one association to Disciplina
	@OneToMany(mappedBy="professor")
	private List<Disciplina> disciplinas;

	public Professor() {
	}

	public List<Disciplina> getDisciplinas() {
		return this.disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public Disciplina addDisciplina(Disciplina disciplina) {
		getDisciplinas().add(disciplina);
		disciplina.setProfessor(this);

		return disciplina;
	}

	public Disciplina removeDisciplina(Disciplina disciplina) {
		getDisciplinas().remove(disciplina);
		disciplina.setProfessor(null);

		return disciplina;
	}

}